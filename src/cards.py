#!python3
import string
import random
from abc import ABCMeta, abstractproperty
from typing import List, Tuple, Union, Dict

__all__: Tuple[str, ...] = ("Deck", "Hand",)

_HOUSES: Dict[str, str] = {
    'S': "Spades",
    'H': "Hearts",
    'C': "Clubs",
    'D': "Diamonds",
    'J': "Joker", # Currently unused, may remove in the future
}

_VALUES: Dict[str, str] = {
    'J': "Jack",
    'Q': "Queen",
    'K': "King",
    'A': "Ace",
}

class CardMixin(metaclass=ABCMeta):
    """
    Adds some useful functionality for dealing with cards
    
    Requires the inheriting class to define a cards-attribute (list)
    """

    houses = "".join(_HOUSES.keys())

    cards = NotImplemented

    def __str__(self):
        """ Returns a human-readable representation of the cards found in self.cards """
        pretty_cards = [
            self.get_card_name(card)
            for card in self.cards
        ]
        return ", ".join(pretty_cards)

    def get_card_name(self, card: str):
        return (
            "Joker" if card == 'J'
            else "{value} of {house}".format(value=_VALUES.get(card[1:], card[1:]), house=_HOUSES[card[0]])
            )

class Hand(CardMixin):
    def __init__(self, player_name: str, cards: List[str] = None) -> None:
        self.name = player_name

        self.cards = cards
        if self.cards is None:
            self.cards = []

    def sort(self): # TODO: Support for jokers
        houses_sorted: Tuple[List[str], ...] = ([], [], [], [])

        for card in self.cards:
            houses_sorted[self.houses.index(card[0])].append(card)
        for house in houses_sorted:
            house.sort()
        temp = []
        for h in houses_sorted:
            temp.extend(h)
        self.cards = temp

class Deck(CardMixin):
    def __init__(self, cards: List[str] = None, card_count = 52) -> None:
        self.houses = "SHCD"
        self.values: Tuple[str, ...] = tuple(list(string.digits[2:]) + ["10", 'J', 'Q', 'K', 'A'])

        self.cards = cards
        if cards is None:
            self.cards = list(self.generate())

    def __repr__(self):
        return "Deck(cards: List[str])"

    def generate(self, n=52, jokers=False):
        
        # Spades, hearts, clubs, diamonds; SHCD
        for house in self.houses:
            for value in self.values:
                yield house + value
        
    def shuffle(self, n=0):
        if n == 0:
            n = random.randint(1,10)
        for _ in range(n):
            random.shuffle(self.cards)
